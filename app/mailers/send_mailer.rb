class SendMailer < ApplicationMailer
	layout 'mailer'

  def mail_send(to_email, subj)
    @greeting = "Hi"

    mail to: to_email, subject: subj
  end
end
