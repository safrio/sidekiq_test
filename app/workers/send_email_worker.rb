class SendEmailWorker
  include Sidekiq::Worker

  def perform(to_email, subj)
  	SendMailer.mail_send(to_email, subj).deliver_now
  end
end
